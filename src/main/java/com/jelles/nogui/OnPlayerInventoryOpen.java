package com.jelles.nogui;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;

public class OnPlayerInventoryOpen implements Listener {
    private NoGUI noGUI;

    public OnPlayerInventoryOpen(NoGUI noGUI) {
        this.noGUI = noGUI;
    }

    @EventHandler
    public void OnPlayerInventoryOpen(InventoryOpenEvent e) {
        if (!(e.getPlayer() instanceof LivingEntity)) return;
        if (!noGUI.isRunning()) return;
        e.getPlayer().sendMessage(noGUI.getConfig().getString("nogui.inventory-open-message"));
        e.setCancelled(true);
    }
}
