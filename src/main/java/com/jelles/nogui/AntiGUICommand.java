package com.jelles.nogui;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;

public class AntiGUICommand implements CommandExecutor {
    private NoGUI noGUI;
    public AntiGUICommand(NoGUI noGUI) {
        this.noGUI = noGUI;
    }
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(args.length != 1) {
            commandSender.sendMessage(noGUI.getConfig().getString("nogui.invalid-command"));
            return false;
        }
        if(!commandSender.hasPermission("nogui.admin")) {
            return false;
        }
        if(args[0].toLowerCase().equals("reload")) {
            noGUI.reloadConfig();
            commandSender.sendMessage(noGUI.getConfig().getString("nogui.reload-command-message"));
            return true;
        } else if (args[0].toLowerCase().equals("start")) {
            if(noGUI.isRunning()) {
                commandSender.sendMessage(noGUI.getConfig().getString("nogui.already-running-message"));
                return false;
            }
            noGUI.setRunning(true);
            commandSender.sendMessage(noGUI.getConfig().getString("nogui.now-running-message"));
            return true;
        } else if (args[0].toLowerCase().equals("stop")) {
            if(!noGUI.isRunning()) {
                commandSender.sendMessage(noGUI.getConfig().getString("nogui.not-running-message"));
                return false;
            }
            noGUI.setRunning(false);
            commandSender.sendMessage(noGUI.getConfig().getString("nogui.stopped-running-message"));
            return true;
        } else {
            commandSender.sendMessage(noGUI.getConfig().getString("nogui.invalid-command"));
            return false;
        }
    }
}
