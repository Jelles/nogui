package com.jelles.nogui;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class NoGUI extends JavaPlugin {
    private boolean Running;

    @Override
    public void onEnable() {
        DefaultConfig();
        getServer().getPluginManager().registerEvents(new OnPlayerInventoryOpen(this), this);
        getCommand("nogui").setExecutor(new AntiGUICommand(this));
    }

    @Override
    public void onDisable() {
        saveDefaultConfig();
    }

    public void DefaultConfig() {
        getConfig().options().copyDefaults();
        saveDefaultConfig();
    }

    public boolean isRunning() {
        return Running;
    }

    public void setRunning(boolean running) {
        Running = running;
    }
}
